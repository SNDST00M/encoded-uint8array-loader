# encoded-uint8array-loader

[![NPM Version][npm-image]][npm-url]

Encoded byte array (`Uint8Array`) loader for webpack.

Optimized vs [bin-loader][github-bin-loader] because output is around 30% smaller.

## Installation

```bash
# npm install encoded-uint8array-loader
```

## Usage

This is the `module.rules` option to inline a `.wasm` file as a `Uint8Array`:

``` javascript
{ test: /\.wasm$/, type: 'javascript/auto', loader: 'encoded-uint8array-loader' }
```

## Credits

Song (github.com/pawsong) for code from their [bin-loader][github-bin-loader]

## License

MIT

[github-bin-loader]: https://github.com/pawsong/bin-loader
[npm-image]: https://img.shields.io/npm/v/encoded-uint8array-loader.svg
[npm-url]: https://npmjs.org/package/encoded-uint8array-loader
